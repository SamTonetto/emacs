;;; Commentary

;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.

(require 'package)
(package-initialize)
(add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/"))
(add-to-list 'package-archives '("marmalade" . "https://marmalade-repo.org/packages/"))

(defconst user-init-dir
  (cond ((boundp 'user-emacs-directory)
         user-emacs-directory)
        ((boundp 'user-init-directory)
         user-init-directory)
        (t "~/.emacs.d/")))


(defun load-user-file (file)
  (interactive "f")
  "Load a file in current user's configuration directory"
  (load-file (expand-file-name file user-init-dir)))

(use-package better-defaults) 

;;; PYTHON SECTION THANKS TO 'b yuksel'
;;; did pip2(3) install elpy, jedi, rope
(elpy-enable)
;; Fixing a key-binding bug in elpy
(define-key yas-minor-mode-map (kbd "C-c k") 'yas-expand)
;;; Fixing another key-binding bug in iedit mode
(define-key global-map (kbd "C-c o") 'iedit-mode)
(setq python-shell-prompt-detect-failure-warning nil)



;;; Python auto-complete
;;;(add-hook 'python-mode-hook 'jedi:setup)
;;;(setq jedi:complete-on-dot t)                 ; optional

;;; ================================================================================
;;; ================================================================================
;;; ================================================================================
;;; ================================================================================
;;; THIS WHOLE SECTION IS THANKS TO YOUTUBE VIDEO BY 'b yuksel'
;;; auto-complete with emacs
(require 'auto-complete)
;;; do default config for auto-complete
(require 'auto-complete-config)
(ac-config-default)
;;; start yasnippet with emacs
(require 'yasnippet)
(yas-global-mode 1)
;;; let's define a functino which initializes auto-complete-c-headers and gets called for c/c++ hooks
(defun my:ac-c-header-init()
  (require 'auto-complete-c-headers)
  (add-to-list 'ac-sources 'ac-source-c-headers)
  (add-to-list 'achead:include-directories '"/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/lib/clang/9.0.0/include")
  (add-to-list 'achead:include-directories '"/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/include/c++/v1")
  (add-to-list 'achead:include-directories '"/usr/local/include")
  )
;now let's call this function from c/c++ hooks
(add-hook 'c++-mode-hook 'my:ac-c-header-init)
(add-hook 'c-mode-hook 'my:ac-c-header-init)
;;; iedit comes installed with a 'bug', which we will now fix
(define-key global-map (kbd "C-c ;") 'iedit-mode)
;;; start flymake-google-ccplint-load
;;; let's define a function for flymake initialization
(defun my:flymake-google-init ()
  (require 'flymake-google-cpplint)
  (custom-set-variables
   '(flymake-google-cpplint-command "/usr/local/bin/cpplint"))
  (flymake-google-cpplint-load)
  )
(add-hook 'c-mode-hook 'my:flymake-google-init)
(add-hook 'c++-mode-hook 'my:flymake-google-init)
;;; in order for cpplint to work, you have to install cpplint externally
;;; do: pip2 search cpplint
;;; do: pip2 install cpplint
;;; then: which cpplint
;;; copy location as input argument to flymake-google-cpplint-load

;;; start google-c-style with emacs
(use-package google-c-style)
(add-hook 'c-mode-common-hook 'google-set-c-style)
(add-hook 'c-mode-common-hook 'google-make-newline-indent)

;; turn on semantic
(semantic-mode 1)
;; define a function which adds semantic as backup to auto-complete
(defun my:add-semantic-to-autocomplete()
  (add-to-list 'ac-sources 'ac-source-semantic))
(add-hook 'c-mode-common-hook 'my:add-semantic-to-autocomplete)
;;; ================================================================================
;;; ================================================================================
;;; ================================================================================
;;; ================================================================================

(set-face-background 'vertical-border "gray")
(set-face-foreground 'vertical-border (face-background 'vertical-border))

;;; indentation style
(setq c-default-style "bsd"
        c-basic-offset 4)

(setq mac-command-modifier 'super)

(require 'vimish-fold)
(vimish-fold-global-mode 1)
(global-set-key (kbd "C-M-x") #'vimish-fold)
(global-set-key (kbd "C-M-d") #'vimish-fold-delete)
(global-set-key (kbd "C-M-c") #'vimish-fold-toggle)
(global-set-key (kbd "C-M-f") #'vimish-fold-toggle-all)
;;;(global-set-key (kbd "M-s-S-<left>") #'vimish-fold)
;;;(global-set-key (kbd "") #'vimish-fold-delete)

(setq monokai-distinct-fringe-background 1)
(load-theme 'monokai t)

;;; bracket coloring
(require 'rainbow-delimiters)
(add-hook 'prog-mode-hook 'rainbow-delimiters-mode)

;;; LOAD CONFIG FILES
(load-user-file "god-init.el")
(load-user-file "eshell-init.el")
(load-user-file "auctex-init.el")
(load-user-file "outline-mode-init.el")
(load-user-file "ace-window-init.el")
(load-user-file "paradox-init.el")
(load-user-file "bell-init.el")


(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(elpy-rpc-python-command "/usr/local/bin/python2.7")
 '(flymake-google-cpplint-command "/usr/local/bin/cpplint")
 '(package-selected-packages
   (quote
    (which-key try elpy flycheck-irony irony-eldoc company-rtags rtags company use-package company-irony google-c-style flymake-cursor flymake-google-cpplint iedit auto-complete-c-headers yasnippet better-defaults irony python-mode pdf-tools vimish-fold jedi monokai-theme rainbow-delimiters auctex paradox god-mode auto-complete ace-window)))
 '(paradox-automatically-star t))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:background nil)))))

