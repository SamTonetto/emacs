;; -*- mode: emacs-lisp; coding: emacs-mule; -*-
;; --------------------------------------------------------------------------
;; Desktop File for Emacs
;; --------------------------------------------------------------------------
;; Created Fri Oct  6 17:13:19 2017
;; Desktop file format version 206
;; Emacs version 24.4.51.2
(setq revive:frame-configuration-to-restore
  '(progn 
     (revive:restore-frame '((tool-bar-position . top) (explicit-name) (icon-name) (bottom-divider-width . 0) (right-divider-width . 0) (top . 23) (left . 0) (unsplittable) (modeline . t) (width . 201) (height . 45) (fringe) (mouse-color . "black") (environment) (visibility . t) (cursor-color . "Red") (background-mode . light) (horizontal-scroll-bars . t) (fullscreen) (alpha) (scroll-bar-width . 15) (cursor-type . box) (auto-lower) (auto-raise) (icon-type) (title) (buffer-predicate) (tool-bar-lines . 1) (menu-bar-lines . 1) (right-fringe . 11) (left-fringe . 3) (line-spacing) (background-color . "White") (foreground-color . "Black") (vertical-scroll-bars . right) (internal-border-width . 0) (border-width . 0) (font . "-*-Monaco-normal-normal-normal-*-12-*-*-*-m-0-iso10646-1") (fontsize . 0)) '(201 45 ((0 0 206 22) (0 22 206 44)) ((nil "*Packages*" 1 1 ((tab "*scratch*" "/Users/samtonetto/Library/Application Support/Aquamacs Emacs/scratch buffer") (tab "test.tex" "/Users/samtonetto/test.tex"))) (nil "*Warnings*" 371 1 nil))))))

;; Global section:
(setq desktop-saved-frameset [frameset 1 (22999 7935 534814 0) (desktop . "206") "samtonetto@Sams-MacBook-Pro.local" nil nil ((((font-backend mac-ct ns) (fontsize . 0) (font . "-*-Monaco-normal-normal-normal-*-12-*-*-*-m-0-iso10646-1") (border-width . 0) (internal-border-width . 0) (vertical-scroll-bars . right) (foreground-color . "Black") (background-color . "White") (line-spacing) (left-fringe . 3) (right-fringe . 11) (menu-bar-lines . 1) (tool-bar-lines . 1) (title) (icon-type) (auto-raise) (auto-lower) (cursor-type . box) (scroll-bar-width . 15) (alpha) (fullscreen) (horizontal-scroll-bars . t) (display-type . color) (background-mode . light) (cursor-color . "Red") (visibility . t) (environment) (mouse-color . "black") (fringe) (frameset--id . "D37C-B269-B79F-33B7") (frameset--mini t . t) (height . 45) (width . 201) (modeline . t) (minibuffer . t) (unsplittable) (left . 0) (top . 23) (right-divider-width . 0) (bottom-divider-width . 0) (icon-name) (display . "Sams-MacBook-Pro.local") (explicit-name) (tool-bar-position . top)) ((min-height . 8) (min-width . 10) (min-height-ignore . 7) (min-width-ignore . 7) (min-height-safe . 2) (min-width-safe . 2) (min-pixel-height . 128) (min-pixel-width . 70) (min-pixel-height-ignore . 98) (min-pixel-width-ignore . 43) (min-pixel-height-safe . 32) (min-pixel-width-safe . 14)) vc (pixel-width . 1440) (pixel-height . 704) (total-width . 206) (total-height . 44) (normal-height . 1.0) (normal-width . 1.0) (combination-limit) (leaf (pixel-width . 1440) (pixel-height . 352) (total-width . 206) (total-height . 22) (normal-height . 0.5) (normal-width . 1.0) (buffer "*Packages*" (selected . t) (hscroll . 0) (fringes 3 11 nil) (margins nil) (scroll-bars 15 3 t nil) (vscroll . 0) (dedicated) (point . 1) (start . 1))) (leaf (last . t) (pixel-width . 1440) (pixel-height . 352) (total-width . 206) (total-height . 22) (normal-height . 0.5) (normal-width . 1.0) (buffer "*Warnings*" (selected) (hscroll . 0) (fringes 3 11 nil) (margins nil) (scroll-bars 15 3 t nil) (vscroll . 0) (dedicated) (point . 371) (start . 1)))))])
(setq desktop-missing-file-warning nil)
(setq tags-file-name nil)
(setq tags-table-list nil)
(setq search-ring nil)
(setq regexp-search-ring nil)
(setq register-alist nil)
(setq file-name-history '("~/test.tex" "~/Downloads/Mathematica 10/again.nfo" "~/Desktop/Research/Code/test/test/eigvecCubeText.dat" "~/Desktop/Research/Code/March2015/HamiltonianEigenvalues/HamiltonianEigenvalues/Run3/timeListText.dat" "~/newtest.tex" "~/filename.tex" "~/foobar"))

;; Buffer section -- buffers listed in same order as in buffer list:
(desktop-create-buffer 206
  "/Users/samtonetto/test.tex"
  "test.tex"
  'latex-mode
  '(auto-fill-mode smart-spacing-mode TeX-PDF-mode bib-cite-minor-mode reftex-mode)
  1
  '(nil nil)
  nil
  nil
  '((indent-tabs-mode) (buffer-file-coding-system . undecided-unix) (case-fold-search . t) (truncate-lines)))

(desktop-create-buffer 206
  "/Users/samtonetto/Library/Application Support/Aquamacs Emacs/scratch buffer"
  "*scratch*"
  'text-mode
  '(visual-line-mode smart-spacing-mode TeX-PDF-mode)
  1
  '(nil nil)
  nil
  nil
  '((buffer-file-coding-system . utf-8) (truncate-lines)))

