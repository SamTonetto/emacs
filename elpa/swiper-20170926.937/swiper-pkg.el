;;; -*- no-byte-compile: t -*-
(define-package "swiper" "20170926.937" "Isearch with an overview. Oh, man!" '((emacs "24.1") (ivy "0.9.0")) :commit "9caa6acc510c04734c2887c1d194fd387b4da114" :url "https://github.com/abo-abo/swiper" :keywords '("matching"))
