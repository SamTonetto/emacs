;;; ============== BIND <escape> to ESC =====================
;;; https://github.com/chrisdone/god-mode/issues/43 SOLUTION TO ESCAPE KEY
(defvar personal/fast-keyseq-timeout 200)

(defun personal/-tty-ESC-filter (map)
  (if (and (equal (this-single-command-keys) [?\e])
           (sit-for (/ personal/fast-keyseq-timeout 1000.0)))
      [escape] map))

(defun personal/-lookup-key (map key)
  (catch 'found
    (map-keymap (lambda (k b) (if (equal key k) (throw 'found b))) map)))

(defun personal/catch-tty-ESC ()
  "Setup key mappings of current terminal to turn a tty's ESC into `escape'."
  (when (memq (terminal-live-p (frame-terminal)) '(t pc))
    (let ((esc-binding (personal/-lookup-key input-decode-map ?\e)))
      (define-key input-decode-map
        [?\e] `(menu-item "" ,esc-binding :filter personal/-tty-ESC-filter)))))

(personal/catch-tty-ESC)
;;; =======================================================


(require 'god-mode)
 (global-set-key (kbd "<escape>") 'god-local-mode)

;;; Change cursor if in god mode
(defun my-update-cursor ()
  (setq cursor-type (if (or god-local-mode buffer-read-only) 'bar 'box)))

(eval-after-load 'god-local-mode
  '(progn
     (add-hook 'god-mode-enabled-hook 'my-update-cursor)
     (add-hook 'god-mode-disabled-hook 'my-update-cursor)))
