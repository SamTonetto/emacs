
;;; OUTLINE MODE
;;; allows hiding of some parts of text file
;;; i.e. folding sections, subsections, chapters
;;; Hide current section: C-c C-o C-l
;;; Move to next unit of doc: C-c C-o C-n
;;; Move to prev unit of doc: C-c C-o C-p
;;; See whole doc again: C-c C-o C-a
(defun turn-on-outline-minor-mode ()
(outline-minor-mode 1))

(add-hook 'LaTeX-mode-hook 'turn-on-outline-minor-mode)
(add-hook 'latex-mode-hook 'turn-on-outline-minor-mode)
(setq outline-minor-mode-prefix "\C-c \C-o") ; Or something else
;;; END OUTLINE MODE
